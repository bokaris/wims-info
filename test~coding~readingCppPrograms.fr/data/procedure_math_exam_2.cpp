#include <iostream>
#include <math.h>

using namespace std;


bool foo(int n) {
    if (n % 2 == 0) return true;
    for (int d = 3; floor(sqrt(n) + 1); d = d + 2)
        if (n % d == 0)
            return false;
    return true;
}

int main() {
    if (foo(21))
        cout << "Oui" << endl;
    else
        cout << "Non" << endl;

    return 0;
}
