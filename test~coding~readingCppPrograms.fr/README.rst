Module WIMS: Compr�hension de programmes C++
============================================

Pr�sentation
------------

Ce module pr�sente � l'�tudiant des programmes qu'il doit lire et
comprendre, afin de deviner quelle en sera la sortie, ou bien l'entr�e
appropri�e pour obtenir 42. Les programmes sont tir�s al�atoirement
d'une collection de programmes group�s par th�mes. Pour rajouter un
nouveau programme il suffit de l'ajouter dans le r�pertoire
idoine. Voir ci-dessous.

Pour l'instant, la collection est constitu�e d'exercices de C++,
donn�s dans le cadre des cours d'introduction � l'informatique `Info
111 <Nicolas.Thiery.name>`_ et Info 121 de la licence MPI de
l'Universit� Paris Sud, mais l'infrastructure est con�ue pour �tre
�tendue � d'autres th�mes et g�n�ralisable � d'autres langages de
programmation.

Ce module est d�velopp� collaborativement sur:
    `<https://gitlri.lri.fr/nthiery/wims-info/tree/master/test~coding~readingCppPrograms.fr>`_


�tendre le module avec de nouveaux programmes
---------------------------------------------

Le r�pertoire `<data/>`_ contient des programmes C++, qui peuvent �tre de deux types.
Pour les programmes du premier type, l'utilisateur doit deviner la sortie du programme.
Ces programmes sont nomm�s sous la forme ``<theme>_<nom>.cpp``.
Les programme du deuxi�me type doivent lire un entier entre
0 et 99 sur l'entr�e standard, et l'utilisateur doit deviner lequel
donne 42; pour l'instant cet entier doit �tre unique.
Ces programmes sont nomm�s ``<theme>_<nom>_input.cpp``
Pour les deux types de programme, le th�me peut �tre constitu� d'un th�me principal et de sous-th�mes, s�par�s par des ``_`` (p. ex. ``loop_for``, ``loop_while``).
Les noms de fichiers ne doivent contenir que des caract�res alphanum�riques et le caract�re ``_``.

En vue d'obtenir un peu de variabilit� les programmes sont transform�s
avant affichage et compilation, de la mani�re suivante:

- Remplacement de I1, I2, I3, I4 par i,j,k,n respectivement (ou une
  permutation de ces derniers);

- Remplacement de D1, D2, D3 par x,y,z respectivement (ou une
  permutation de ces derniers);

- Remplacement de F1 par f.

Ces remplacements sont fait de mani�re purement textuelle. Ainsi,
�AI1� sera remplac� par, par exemple, �Ai�.

Dans la version actuelle, le remplacement est fait statiquement avant
le chargement du module dans WIMS (�diter la premi�re ligne du
Makefile pour changer/compl�ter la liste des remplacements). � terme
ce remplacement sera fait dynamiquement par WIMS, avec une permutation
diff�rente � chaque ex�cution de l'exercice.

.. TODO:: Am�liorer l'ent�te du Makefile pour que ne soient remplac�s que les mots complets.

L'infrastructure du module est implant�e dans
`<./src/cpp/read_program.cpp>`_.

.. WARNING::

    Dans l'�tat actuel de l'exercice, l'utilisateur ne peut pas
    rentrer de r�ponse vide. Il faut donc que tous les programmes du
    premier type affiche au moins un caract�re. �viter les caract�res
    unicode compliqu�s aussi :-)

Conventions de style
--------------------

Pour faciliter une lecture rapide, il est bon pour les �l�ves que tous
les programmes soient �crits dans un style coh�rent (indentation,
espaces, ...). Pour donner cette coh�rence, ce module utilise `astyle
<http://astyle.sourceforge.net/>`_ pour reformater syst�matiquement
tous les programmes. Voir:

    make reformat

Les choix suivants, forc�ment un peu arbitraire, ont �t� fait. Voir la
documentation de astyle pour les d�tails:

    --style=java --indent=spaces --indent=spaces=4 --indent-switches --indent-namespaces
    � voir: --indent-modifiers
    --indent-col1-comments: � voir en pratique si cela devient g�nant
    --min-conditional-indent=0
    --pad-oper
    --pad-header
    --align-pointer=name     Sous r�serve que, dans "char *f, g", le * ne s'applique qu'� f
    --align-reference=name
    --keep-one-line-statements
    --convert-tabs
    --close-templates
    --break-after-logical
    --max-code-length=78
    --recursive

Pour utiliser ce module dans un contexte utilisant d'autres
conventions, il serait tout � fait possible de reformater le code
selon d'autres conventions, soit au vol dans wims, ou au moment moment
de la fabrication de la version static.

Cr�ation automatique des exercices de type OEF
----------------------------------------------

L'ent�te du fichier `src/cpp/read_program.cpp` permet de d�finir les exercices de type OEF qui vont �tre construits automatiquement par WIMS lors de l'import du module (voir section suivante).
L'ent�te doit �tre de la forme ``target=<cible1> <cible2> <cible3> ...``.
WIMS cr�e un exercice OEF par cible sous la forme d'un fichier `src/<cible>.oef`.
Lors de chaque ex�cution de l'exercice OEF `<cible>.oef`, un programme est choisi al�atoirement parmi tous les programmes de `data/static` dont le nom commence par ``<cible>``.
Il est ainsi possible de d�finir un exercice OEF prenant en compte tous les programmes ayant un certain th�me, ou un certain th�me et sous-th�me, ou simplement un seul exercice (dans ce dernier cas, la cible doit �tre le nom du programme complet, sans l'extension ``.cpp``).

Pour chaque cible, le fichier `src/cpp/read_program.cpp` doit contenir les d�finitions suivantes:

``#if defined TARGET_<cible>
#define TARGET <cible>
#define TARGET_FR <Titre de l'exercice>
#endif``

Importer ou mettre � jour le module sur WIMS
--------------------------------------------

Lancer `make archive` dans ce r�pertoire. Cela cr�� une archive:

    /tmp/modtool-test~coding~readingCppPrograms.fr.tgz

Charger l'archive dans son compte modtools sur WIMS avec:

    wims.u-psud.fr -> serveur de l'universit� -> Accueil WIMS -> modtools -> login -> Restauration

TODO
----

- data/function_if.cpp:
  le contenu de la fonction semble trop compliqu� par rapport � ce que
  l'on veut tester
- data/procedure-simplest_1.cpp: a conserver?
- data/static/procedure_math_exam_1: ne compile pas

Documentation g�n�rique des modules WIMS
========================================

Pour installer des exercices OEF dans un module :

 1. Cr�er un module de type OEF

 2. Pour chaque exercice, cr�er dans le module un nouveau fichier
    d'extension oef dans le r�pertoire src/ (exemple : src/euclide.oef).
    Une zone de  texte appara�t ; y �crire (ou coller) le texte source
    de l'exercice. Enregistrer les changements.

 3. Tester le module.

 4. Modifier � son go�t intro.phtml et endhook.phtml,
     et tester � nouveau.


%%%%%% Pour utiliser directement le module sur un serveur WIMS local,

1- Mettre  le template � la bonne place,
en changeant le nom.

2. Modifier intro.phtml

3. Modifier le fichier INDEX.

4. Installer les fichiers sources.

5. Ex�cuter le script $wims_home/other/bin/src2def. (Cette �tape
   doit �tre r�p�t�e � chaque fois que les fichiers sources sont modifi�s).


